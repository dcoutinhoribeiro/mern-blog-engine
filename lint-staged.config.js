module.exports = {
  'src/backend/**/*.{js,css,scss,md}': ['prettier --write'],
  'src/backend/**/*.{ts,tsx}': ['prettier --write', 'eslint src/backend/**/*.{ts,tsx}  --fix --max-warnings 0'],
};
