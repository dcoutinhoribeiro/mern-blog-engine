import { Route, Routes } from 'react-router-dom';
import { useContext } from 'react';

import {  PostDataTable, Editor, } from './../../views'
import ApplicationContext from '../../contexts';
import { NotFound } from '../../components';

const Dashboard: React.FC = function () {
  const { posts } = useContext(ApplicationContext);

  return (
    <section>
      <header className="py-6">
        <h1 className="text-2xl font-bold tracking-tight">Blog Dashboard</h1>
      </header>
      <Routes>
        <Route path="/editor" element={<Editor />}>
          <Route path=":category/:title" element={null} />
        </Route>
        <Route path="/" element={<PostDataTable posts={posts} />} />
        <Route path="*" element={<NotFound toDashboard={true} />} />
      </Routes>
    </section>
  );
};

export default Dashboard;
