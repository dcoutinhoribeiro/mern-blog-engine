import { BrowserRouter, Routes, Route } from 'react-router-dom';

import { Header, NotFound } from '../../components';
import { Post, Home } from '../../views';
import { Dashboard } from '../../layout'

const Layout: React.FC = function () {
  return (
    <BrowserRouter>
      <div className="max-w-3xl mx-auto px-4 sm:px-6 xl:max-w-5xl xl:px-0">
        <Header />
      </div>
      <div className="max-w-3xl mx-auto px-4 sm:px-6 xl:max-w-5xl xl:px-0">
        <main>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/admin/*" element={<Dashboard />}/>
            <Route path="/:category/:title" element={<Post />} />
            <Route path="*" element={<div><NotFound /></div>} />
          </Routes>
        </main>
      </div>
    </BrowserRouter>
  );
};

export default Layout;
