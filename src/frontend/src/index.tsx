import React from 'react';
import ReactDOM from 'react-dom';

import BaseApp from './containers';

import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <BaseApp />
  </React.StrictMode>,
  document.getElementById('root'),
);
