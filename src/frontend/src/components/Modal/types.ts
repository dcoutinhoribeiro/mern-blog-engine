import { MouseEventHandler } from "react";

export interface ModalProps {
    text: string; 
    title: string;
    cancelText: string; 
    confirmText: string;
    onCancel: MouseEventHandler<HTMLButtonElement>;
    onConfirm: MouseEventHandler<HTMLButtonElement>;
}