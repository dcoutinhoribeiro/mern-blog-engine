import { Database } from 'grommet-icons';

const NoData: React.FC = () => (
  <div className="container grid place-items-center py-5 text-gray-700">
    <div className="max-w-md items-center flex flex-col spacey-4">
      <div className='mb-4'>
        <Database size='large' />
      </div>
      <p className="text-md font-light  mb-8">There&apos;s no data to show</p>
    </div>
  </div>
);

export default NoData;
