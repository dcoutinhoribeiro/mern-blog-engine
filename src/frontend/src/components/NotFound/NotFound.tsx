import { Link } from 'react-router-dom';

const NotFound: React.FC<{ toDashboard?: boolean }> = ({ toDashboard }) => (
  <div className="container flex flex-col md:flex-row pt-6  text-gray-700">
    <div className="max-w-md">
      <p className="text-2xl md:text-3xl font-light leading-normal mb-8">Sorry we couldn&#39;t find this page. </p>
      {toDashboard ? (
        <Link className="text-blue-600 hover:text-blue-800 " aria-label="Go back to the dashboard" to={`/admin`}>
          ← Back to the dashboard
        </Link>
      ) : (
        <Link className="text-blue-600 hover:text-blue-800 " aria-label="Go back to the blog" to={`/`}>
          ← Back to the blog
        </Link>
      )}
    </div>
  </div>
);

export default NotFound;
