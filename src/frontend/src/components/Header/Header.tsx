import { Link } from 'react-router-dom'
import logo from './../../assets/logo.svg'

const Header: React.FC = function () {
  return (
    <header className="flex justify-between items-center py-10">
        <div>
          <Link to='/' className='flex items-baseline'>
            <img src={logo} alt="logo" />
            <span className='font-medium tracking-widest uppercase ml-2 text-2xl text-gray-500 hover:text-gray-600'>
              Blog
            </span>
          </Link>
        </div>
        <div className="text-base leading-5">
          <Link to='/admin' className="font-medium text-gray-500 hover:text-gray-700">
            Dashboard →
          </Link>
        </div>
    </header>
  );
};

export default Header;
