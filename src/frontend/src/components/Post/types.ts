import { Post } from '../../contexts';

export type PostProps = {
  post?: Post;
};
