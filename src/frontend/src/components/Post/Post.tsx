import { useMemo } from 'react';
import { Link } from 'react-router-dom';
import RichMarkdownEditor from 'rich-markdown-editor';
import { LONG, NUMERIC } from '../../utils';

import { PostProps } from './types';

const Post: React.FC<PostProps> = function (props) {
  const { post } = props;

  const formattedDate = useMemo(() => {
    if (!post?.timestamp) return;

    const config = {
      day: NUMERIC,
      month: LONG,
      year: NUMERIC,
    };

    const date = new Date(post?.timestamp);

    return date.toLocaleDateString('en-US', config);
  }, [post?.timestamp]);

  return (
    <li className="py-12">
      <article className="space-y-2 xl:grid xl:grid-cols-4 xl:space-y-0 xl:items-baseline">
        <dl>
          <dt className="sr-only">Published on</dt>
          <dd className="text-base font-medium text-gray-500">
            <time dateTime={String(post?.timestamp) || ''}>{formattedDate}</time>
          </dd>
          <dd className="text-base font-medium text-gray-500">
            On <span className="text-gray-900">{post?.category}</span>
          </dd>
        </dl>
        <div className="space-y-5 xl:col-span-3">
          <div className="space-y-6">
            <h2 className="text-2xl font-bold tracking-tight">
              <Link className="text-gray-900" to={`/${post?.encodedCategory}/${post?.encodedTitle}`}>
                {post?.title}
              </Link>
            </h2>
            <div className="prose max-w-none text-gray-500">
              <div
                className={`prose max-h-48 ${
                  (post?.body?.length || -1) >= 160 ? 'background-special' : ''
                } overflow-hidden max-w-none`}
              >
                <RichMarkdownEditor readOnly defaultValue={post?.body} />
              </div>
            </div>
          </div>
          <div className="text-base font-medium">
            <Link
              className="text-blue-600 hover:text-blue-800 "
              aria-label={`Read "${post?.title}"`}
              to={`/${post?.encodedCategory}/${post?.encodedTitle}`}
            >
              Read more →
            </Link>
          </div>
        </div>
      </article>
    </li>
  );
};

export default Post;
