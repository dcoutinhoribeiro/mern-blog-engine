import Header from './Header';
import Post from './Post';
import NotFound from './NotFound'
import Modal from './Modal'
import NoData from './NoData'

export { Header, Post, NotFound, Modal, NoData };
