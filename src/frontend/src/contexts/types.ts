import { HandleSubmitPostType, HandleDeletePostType } from '../containers/BaseApp/types';

export interface Post {
  title: string;
  body: string;
  author: string;
  timestamp: Date;
  category: string;
  _id: string;
  encodedTitle: string; 
  encodedCategory: string;
}

export interface ApplicationContextType {
  posts: Post[];
  handleSubmitPost: HandleSubmitPostType;
  handleDeletePost: HandleDeletePostType;
  fetching: boolean;
}
