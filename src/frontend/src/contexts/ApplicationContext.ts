import { createContext } from 'react';

import { ApplicationContextType } from './types';

const ApplicationContext = createContext<ApplicationContextType>({} as ApplicationContextType);

export default ApplicationContext;
