import { Post } from '../contexts';

export const encodeString: (str: string) => string = (str: string) =>
  encodeURI(
    str
      .replace(/[^a-zA-Z ]/g, '')
      .replace(/\s+/g, '-')
      .toLowerCase(),
  );

export const getPostWithEncoded: (post: Partial<Post>) => Partial<Post> = (post) => ({
  ...post,
  encodedCategory: encodeString(post.category || ''),
  encodedTitle: encodeString(post.title || ''),
});

export const LONG = 'long' as const;
export const NUMERIC = 'numeric' as const;
