import { useMemo, useState, useEffect, useCallback } from 'react';

import ApplicationContext, { ApplicationContextType, Post } from '../../contexts';
import { Layout } from '../../layout';
import { getPostWithEncoded } from '../../utils';
import { HandleDeletePostType, HandleSubmitPostType } from './types';

const BaseApp: React.FC = function () {
  const [posts, setPosts] = useState<Post[]>();
  const [fetching, setFetching] = useState(false);

  const handleDeletePost: HandleDeletePostType = useCallback(async (postId) => {
    const postToBeDeleted = posts?.find(post => post?._id === postId);

    if(!postToBeDeleted) return false;

     const data = await fetch(`/api/post/${postId}`, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
    }).then((response) => response.status !== 200 ? 'failure' : 'success');

    if(data !== 'success') return false;

    setPosts(posts?.filter(post => post?._id !== postId));

    return true;
  }, [setPosts, posts])

  const handleSubmitPost: HandleSubmitPostType = useCallback(async (post, postId) => {
    if (!post) return false;

    if (postId) {
      const data = await fetch(`/api/post/${postId}`, {
        method: 'PATCH',
        body: JSON.stringify(post), 
        headers: {
          Accept: 'application/json, text/plain, */*',
          'Content-Type': 'application/json',
        },
      }).then((response) => response.json());

      if (!data || data.errors) return false;

      setPosts(
        (oldPosts) =>
          oldPosts &&
          oldPosts
            .map((post) => (post?._id === postId ? { ...post, ...data } : post))
            .sort((a, b) => +new Date(b.timestamp) - +new Date(a.timestamp)),
      );
    } else {
      const data = await fetch('/api/post', {
        method: 'POST',
      body: JSON.stringify(post), 
        headers: {
          Accept: 'application/json, text/plain, */*',
          'Content-Type': 'application/json',
        },
      }).then((response) => response.json());

      if (!data || data.errors) return false;

      setPosts((oldPosts) =>
        oldPosts ? [...oldPosts, getPostWithEncoded(data)].sort((a, b) => +new Date(b?.timestamp || "") - +new Date(a?.timestamp || "")) : [data],
      );
    }
    return true;
  }, [setPosts]);

  useEffect(() => {
    if (posts !== undefined) return;

    setFetching(true);

    fetch('/api/post')
      .then((response) => response.json())
      .then((data: Post[]) => {
        setPosts(data.sort((a, b) => +new Date(b.timestamp) - +new Date(a.timestamp)));
        setFetching(false);
      });
  }, [posts]);

  const providerData = useMemo<ApplicationContextType>(
    () =>
      ({
        posts,
        fetching,
        handleSubmitPost, 
        handleDeletePost,
      } as ApplicationContextType),
    [fetching, posts, handleSubmitPost, handleDeletePost],
  );

  return (
    <div className="antialiased">
      <div className="max-w-3xl mx-auto px-4 sm:px-6 xl:max-w-5xl xl:px-0">
        <ApplicationContext.Provider value={providerData}>
          <Layout />
        </ApplicationContext.Provider>
      </div>
    </div>
  );
};

export default BaseApp;
