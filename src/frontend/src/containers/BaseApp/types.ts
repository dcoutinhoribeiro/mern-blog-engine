import { Post } from '../../contexts';

export type HandleSubmitPostType = (post: Partial<Post>, postId?: string) => Promise<boolean>;
export type HandleDeletePostType = (postId?: string) => Promise<boolean>;
