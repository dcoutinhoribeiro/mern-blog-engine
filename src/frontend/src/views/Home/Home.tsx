import { useContext } from 'react';

import ApplicationContext from '../../contexts';
import { Post } from '../../components';
import { config } from '../../config';

const Home: React.FC = function () {
  const { posts } = useContext(ApplicationContext);

  return (
    <div className="divide-y divide-gray-200">
      <div className="pt-6 pb-8 space-y-2 md:space-y-5">
        <h1 className="text-3xl font-extrabold text-gray-900 tracking-tight sm:text-4xl md:text-[4rem] md:leading-[3.5rem]">
          Latest
        </h1>
        <p className="text-lg text-gray-500" dangerouslySetInnerHTML={{ __html: config.description }}></p>
      </div>
      {posts && posts.length >= 1 ? (
        <ul className="divide-y divide-gray-200">
          {posts.map((post) => (
            <Post post={post} key={`${post?.encodedCategory}/${post?.encodedTitle}`} />
          ))}
        </ul>
      ) : (
        <p className="text-2xl md:text-3xl font-light leading-normal py-8">I&apos;m sorry, there&apos;s nothing here yet :( </p>
      )}
    </div>
  );
};

export default Home;
