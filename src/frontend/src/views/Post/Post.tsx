import { useContext, useMemo } from 'react';
import { Link, useParams } from 'react-router-dom';
import RichMarkdownEditor from 'rich-markdown-editor';
import { NotFound } from '../../components/';
import { LONG, NUMERIC } from '../../utils';

import ApplicationContext from './../../contexts';

const Post: React.FC = function () {
  const { category, title } = useParams();
  const { posts } = useContext(ApplicationContext);

  const post = useMemo(
    () => posts?.find((post) => post.encodedCategory === category && post.encodedTitle === title),
    [posts, category, title],
  );

  const previousPost = useMemo(() => {
    if (!posts || posts.length < 2) return undefined;

    const currentPostIndex = posts.findIndex((_post) => _post?._id === post?._id);

    if((currentPostIndex + 1 ) % posts.length === 0) return undefined;

    const previousPostIndex = (currentPostIndex + 1 ) % posts.length;

    return posts[previousPostIndex];
  }, [posts, post]);

  const formattedDate = useMemo(() => {
    if (!post?.timestamp) return;
    
    const config = {
      weekday: LONG,
      day:  NUMERIC,
      month: LONG,
      year: NUMERIC,
    };
    const date = new Date(post?.timestamp);

    return date.toLocaleDateString('en-US', config);
  }, [post?.timestamp]);

  return (
    <article className="xl:divide-y xl:divide-gray-200">
      {post ? (
        <>
          <header className="pt-6 xl:pb-10">
            <div className="space-y-1 text-center">
              <dl className="space-y-10">
                <div>
                  <dt className="sr-only">Published on</dt>
                  <dd className="text-base leading-6 font-medium text-gray-500">
                    <time dateTime={String(post?.timestamp)}>{formattedDate}</time>
                  </dd>
                </div>
              </dl>
              <div>
                <h1 className="text-3xl font-extrabold text-gray-900 tracking-tight sm:text-4xl md:text-5xl md:leading-[3.5rem]">
                  {post?.title}
                </h1>
                <p className="text-xs leading-6 font-medium text-gray-500">
                  Published by <span className="text-gray-900">{post?.author}</span> on <span className="text-gray-900">{post?.category}</span>
                </p>
              </div>
            </div>
          </header>
          <div
            className="divide-y xl:divide-y-0 divide-gray-200 xl:grid xl:grid-cols-4 xl:gap-x-6 pb-16 xl:pb-20"
            style={{ gridTemplateRows: 'auto 1fr' }}
          >
            <div className="divide-y divide-gray-200 xl:pb-0 xl:col-span-3 xl:row-span-2">
              <div className="max-w-none overflow-x-hidden prose py-8">
                <RichMarkdownEditor readOnly value={post?.body} />
              </div>
            </div>
            <footer className="text-sm font-medium divide-y divide-gray-200 xl:col-start-1 xl:row-start-2">
              {previousPost && (
                <div className="space-y-8 py-8">
                  <div>
                    <h2 className="text-xs leading-5 tracking-wide uppercase text-gray-500">Previous Article</h2>
                    <div className="text-blue-600 hover:text-blue-800">
                      <Link to={`/${previousPost.encodedCategory}/${previousPost.encodedTitle}`}>
                        {previousPost.title}
                      </Link>
                    </div>
                  </div>
                </div>
              )}
              <div className="pt-8">
                <Link className="text-blue-600 hover:text-blue-800" to="/">
                  ← Back to the blog
                </Link>
              </div>
            </footer>
          </div>
        </>
      ) : (
        <NotFound />
      )}
    </article>
  );
};

export default Post;
