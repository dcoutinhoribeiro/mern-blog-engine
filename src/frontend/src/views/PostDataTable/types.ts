import { Post } from "../../contexts";

export interface PostDataTableProps { 
    posts?: Post[];
}