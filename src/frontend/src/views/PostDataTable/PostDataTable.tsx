import { useState, useCallback, useContext } from 'react';
import { Link } from 'react-router-dom';
import { Edit, Trash } from 'grommet-icons';

import ApplicationContext from '../../contexts';
import { PostDataTableProps } from './types';
import { Modal, NoData } from '../../components';

const PostDataTable: React.FC<PostDataTableProps> = (props) => {
  const { posts } = props;

  const { handleDeletePost } = useContext(ApplicationContext);

  const [error, setError] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [selectedPostId, setSelectedPostId] = useState<string>();

  const handleDelete = useCallback(async () => {
    if (!selectedPostId) return;

    const ok = await handleDeletePost(selectedPostId);
    
    return ok;
  }, [selectedPostId, handleDeletePost]);

  return (
    <div className="divide-y divide-gray-200">
      {showModal && (
        <Modal
          title={'Delete Post'}
          cancelText="Cancel"
          confirmText="Confirm"
          text={
            error
              ? 'There was a error deleting your post, please try again.'
              : 'Are you sure you want to delete this post?'
          }
          onConfirm={async () => {
            const ok = await handleDelete();
            if (ok) {
              setShowModal(false);
              setError(false);
            } else setError(true);
          }}
          onCancel={() => setShowModal(false)}
        />
      )}
      <div className="space-y-8 py-4">
        <div className="flex justify-between align-center">
          <h2 className="text-xs leading-5 tracking-wide uppercase text-gray-500">Post List</h2>
          <Link to="editor" className="text-blue-600 hover:text-blue-800 " aria-label="Go back to the posts list">
            New Post →
          </Link>
        </div>
      </div>
      {posts && posts.length >= 1 ? (
        <div className="overflow-x-auto">
          <table className="table w-full mt-4 table-fixed">
            <colgroup>
              <col span={1} style={{ width: '20%' }} />
              <col span={1} style={{ width: '20%' }} />
              <col span={1} style={{ width: '20%' }} />
              <col span={1} style={{ width: '20%' }} />
              <col span={1} style={{ width: '20%' }} />
            </colgroup>
            <thead>
              <tr>
                <th>Creation Date</th>
                <th className="w-3">Title</th>
                <th>Category</th>
                <th>Author</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {posts.map((post) => (
                <tr className="group hover:cursor-pointer" key={`${post?.encodedCategory}/${post?.encodedTitle}`}>
                  <td className="group-hover:text-gray-800 truncate">{new Date(post?.timestamp).toLocaleDateString('en-us')}</td>
                  <td className="group-hover:text-gray-800 truncate">{post?.title}</td>
                  <td className="group-hover:text-gray-800 truncate">{post?.category}</td>
                  <td className="group-hover:text-gray-800 truncate">{post?.author}</td>
                  <td style={{ width: '173px' }}>
                    <div className="flex justify-start">
                      <Link
                        aria-label="Edit this post"
                        to={`editor/${post?.encodedCategory}/${post?.encodedTitle}`}
                        className="btn btn-xs btn-outline group mr-2"
                      >
                        Edit <Edit size="small" className="ml-2" />
                      </Link>
                      <button
                        onClick={() => {
                          setSelectedPostId(post?._id);
                          setShowModal(true);
                        }}
                        aria-label="Delete this post"
                        className="btn btn-xs btn-outline group"
                      >
                        Delete <Trash size="small" className="ml-2" />
                      </button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      ) : (
        <NoData />
      )}
    </div>
  );
};

export default PostDataTable;
