import { useState, useCallback, useContext, useMemo, FormEvent, useEffect } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import RichTextEditor from 'rich-markdown-editor';
import { Refresh, Save } from 'grommet-icons';
import DatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';

import ApplicationContext, { Post } from '../../contexts';
import { encodeString } from '../../utils';
import { config } from '../../config';

const Editor: React.FC = function () {
  const [triggerRequiredValidation, setTriggerRequiredValidation] = useState(false);
  const [formValues, setFormValues] = useState<Partial<Post>>({});
  const { handleSubmitPost, posts } = useContext(ApplicationContext);
  const [error, setError] = useState(false);
  const [postId, setPostId] = useState<string>()

  const { title, category } = useParams();
  const navigate = useNavigate();

  const initialValues: Partial<Post> | undefined = useMemo(() => {
    const post = posts?.find((post) => post.encodedCategory === category && post.encodedTitle === title);
    return post
      ? {
          ...post,
          timestamp: (() => {
            if (!post?.timestamp) return new Date();
            const date = new Date(post?.timestamp);
            return date;
          })(),
        }
      : undefined;
  }, [posts, title, category]);

  useEffect(() => {
    if (!initialValues) return;
    setFormValues({
      body: initialValues.body,
      title: initialValues.title,
      timestamp: initialValues.timestamp,
      category: initialValues.category,
      author: initialValues.author
    });
    setPostId(initialValues._id);
  }, [initialValues]);

  const uniqueConstraint = useMemo(
    () =>
      !initialValues && formValues?.title && formValues?.category
        ? posts?.some(
            (post) =>
              post?.encodedTitle === encodeString(formValues.title || '') &&
              post?.encodedCategory === encodeString(formValues.category || ''),
          )
        : false,
    [posts, formValues, initialValues],
  );

  const required = useMemo(() => {
    const entries = Object.entries(formValues);
    return triggerRequiredValidation && !(entries.every((entry) => entry[1]) && entries.length === 5);
  }, [formValues, triggerRequiredValidation]);

  const handleChange: (e: FormEvent) => void = useCallback(
    (e) => {
      if (!e?.target) return;
      const { name, value } = e?.target as HTMLInputElement;
      setFormValues((oldFormValues) => ({
        ...oldFormValues,
        [name]: !value || value === null || value === '' ? undefined : value,
      }));
    },
    [setFormValues],
  );

  const handleSubmit: (e: FormEvent) => void = useCallback(
    async (e) => {
      e.preventDefault();
      const entries = Object.entries(formValues);

      if (!(entries.every((entry) => entry[1]) && entries.length === 5)) return false;

      const ok = await handleSubmitPost(formValues, postId);

      if (!ok) {
        setError(true);
      } else {
        setError(false);
        navigate('/admin');
      }
    },
    [formValues, handleSubmitPost, navigate, postId],
  );

  const PostBody = useCallback(
    () => (
      <RichTextEditor
        defaultValue={initialValues?.body || ''}
        onChange={(value) => {
          const actualValue = value();
          setFormValues((oldFormValues) => ({
            ...oldFormValues,
            body: !actualValue || actualValue === null || actualValue === '<br>' ? undefined : actualValue,
          }));
        }}
        className="p-1 cursor-text"
        placeholder="You can type here..."
      />
    ),
    [setFormValues, initialValues],
  );

  return (
    <div className="divide-y divide-gray-200">
      <div className="space-y-8 py-4">
        <div className="flex justify-between align-center">
          <div>
            <h2 className="text-xs leading-5 tracking-wide uppercase mb-1 text-gray-500">New Post</h2>
            <p className="label-text-alt text-warning">All fields are required.</p>
            {error && (
              <p className="label-text-alt text-error">There was an error saving your post, please try again.</p>
            )}
          </div>
          <Link to="/admin" className="text-blue-600 hover:text-blue-800 " aria-label="Go back to the posts list">
            ← Post List
          </Link>
        </div>
      </div>
      <div className="grid lg:space-x-8 py-4 grid-cols-12">
        <div className="col-span-12 lg:col-span-8 mb-8">
          <div
            className={`form-control w-full divide-x ${!formValues?.body && required ? 'divide-warning' : 'divide-gray-200'}`}
          >
            <div className="pb-4">
              <label className="label-text block mb-1">
                Post Body {!formValues?.body && required && <small className="text-error">*</small>}
              </label>
              <p className="label-text-alt">You can start typing in the space below to compose your post.</p>
            </div>
            {<PostBody />}
          </div>
        </div>
        <div className="col-span-12 lg:col-span-4">
          <form
            onSubmit={(e) => {
              setTriggerRequiredValidation(true);
              handleSubmit(e);
            }}
          >
            <div className="form-control w-full">
              <label className="label" htmlFor="title">
                <span className="label-text">
                  Post Title {!formValues?.title && required && <small className="text-error">*</small>}
                </span>
              </label>
              <input
                disabled={initialValues !== undefined}
                onChange={handleChange}
                value={formValues?.title || ''}
                type="text"
                name="title"
                placeholder="Title"
                className={`input input-md input-bordered ${
                  (!formValues?.title && required) || uniqueConstraint ? 'input-error' : ''
                }`}
              />
              <label className="label">
                <span className={`label-text-alt ${uniqueConstraint ? 'text-error' : ''}`}>
                  The post title must be unique within the post&apos;s category.
                </span>
              </label>
            </div>
            <div className="form-control w-full">
              <label className="label" htmlFor="author">
                <span className="label-text">
                  Post Author {!formValues?.author && required && <small className="text-error">*</small>}
                </span>
              </label>
              <input
                onChange={handleChange}
                value={formValues?.author || ''}
                type="text"
                name="author"
                placeholder="Author"
                className={`input input-md input-bordered ${!formValues?.author && required ? 'input-error' : ''}`}
              />
            </div>
            <div className="form-control w-full">
              <label className="label" htmlFor="category">
                <span className="label-text">
                  Post Category {!formValues?.category && required && <small className="text-error">*</small>}
                </span>
              </label>
              <select
                disabled={initialValues !== undefined}
                onChange={handleChange}
                value={formValues?.category || ''}
                name="category"
                className={`select select-bordered w-full ${!formValues?.category && required ? 'select-error' : ''}`}
              >
                <option value={''} disabled>
                  Choose your category
                </option>
                {config?.categories.map((category) => (
                  <option key={category} value={category}>
                    {category}
                  </option>
                ))}
              </select>
            </div>
            <div className="form-control w-full">
              <label className="label" htmlFor="name">
                <span className="label-text">
                  Post Creation Date {!formValues?.timestamp && required && <small className="text-error">*</small>}
                </span>
              </label>
              <DatePicker
                name="timestamp"
                selected={formValues?.timestamp || null}
                className={`input input-md w-full input-bordered ${
                  !formValues?.timestamp && required ? 'input-error' : ''
                }`}
                onChange={(value) =>
                  setFormValues((oldFormValues) => ({
                    ...oldFormValues,
                    timestamp: value === null ? undefined : (value as Date),
                  }))
                }
              />
            </div>
            <div className="flex flex-row py-4 w-full justify-start">
              <button
                onClick={() => setFormValues({ category: '' })}
                aria-label="Save post"
                className="btn btn-md btn-outline group mr-2"
              >
                Reset <Refresh size="medium" className="ml-2" />
              </button>
              <button type="submit" aria-label="Reset post" className={`btn btn-md btn-outline group`}>
                Save <Save size="medium" className="ml-2" />
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Editor;
