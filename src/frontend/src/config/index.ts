const config = {
  categories: ['News', 'Tutorials', 'Documentation', 'Projects', 'Announcements', 'Trivia'],
  description: 'All the latest <a class=\' underline \' href=\'https://www.Able.co\'>Able.co</a> news, straight from the team.'
};

export { config };
