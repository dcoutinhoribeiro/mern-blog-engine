const colors = require('tailwindcss/colors');

module.exports = {
  darkMode: false, // or  'media' or 'class'
  mode: 'jit',
  purge: ['./public/**/*.html', './src/**/*.{js,jsx,ts,tsx,vue, .json}'],
  theme: {
    fontFamily: {
      sans: ['Inter', 'sans-serif'],
    },
    colors: {
      gray: colors.trueGray,
      white: colors.white,
      blue: colors.blue,
    },
  },
  variants: {
    extend: {
      textColor: ['hover'],
      border: ['last'],
      grayScale: ['hover'],
    },
  },
  plugins: [require('daisyui'), require('@tailwindcss/typography')],
};
