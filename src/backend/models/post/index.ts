import { Schema, model } from 'mongoose';
import { Post } from './types';

const schema = new Schema<Post>({
  title: { type: String, required: true },
  author: { type: String, required: true },
  category: { type: String, required: true },
  timestamp: { type: Date, required: true },
  body: { type: String, required: true },
  image: { type: String, required: false },
});

const PostModel = model<Post>('Post', schema);

export type { Post } from './types'
export default PostModel;