export interface Post {
    title: string; 
    encodedTitle: string;
    author: string; 
    timestamp: Date; 
    body: string; 
    image?: string;
    category: string;
    encodedCategory: string;
}
  