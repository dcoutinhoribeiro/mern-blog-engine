import dotenv from 'dotenv';
import mongoose from 'mongoose';

dotenv.config();

async function connectDB() {
  try {
    await mongoose.connect(`mongodb://${process.env.MONGODB_HOST}/devskiller-blog-engine`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
  } catch (error) {
    process.exit(1);
  }
}

export default connectDB;
