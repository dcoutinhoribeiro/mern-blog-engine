import { createPost, deletePost, updatePost, getPosts } from './post'

export {
    createPost, deletePost, updatePost, getPosts,
}