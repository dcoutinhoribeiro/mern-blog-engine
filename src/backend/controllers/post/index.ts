import asyncHandler from 'express-async-handler';
import { PostModel } from './../../models/';

export const encodeString: (str: string) => string = (str: string) =>
  encodeURI(
    str
      .replace(/[^a-zA-Z ]/g, '')
      .replace(/\s+/g, '-')
      .toLowerCase(),
  );

export const getPosts = asyncHandler(async (request, response) => {
  const posts = await PostModel.find({}).lean();

  try {
    response.send(
      posts.map((post) => ({
        ...post,
        encodedTitle: encodeString(post.title),
        encodedCategory: encodeString(post.category),
      })),
    );
  } catch (error) {
    response.status(500).send(error);
  }
});

export const createPost = asyncHandler(async (request, response) => {
  const post = new PostModel(request.body);
  try {
    await post.save();
    response.send(post);
  } catch (error) {
    response.status(500).send(error);
  }
});

export const updatePost = asyncHandler(async (request, response) => {
  try {
    const post = await PostModel.findByIdAndUpdate(request.params.id, request.body);

    if (!post) response.status(404).send('No item found');
    response.status(200).send(post);
  } catch (error) {
    response.status(500).send(error);
  }
});

export const deletePost = asyncHandler(async (request, response) => {
  try {
    const post = await PostModel.findByIdAndDelete(request.params.id);

    if (!post) response.status(404).send('No item found');
    response.status(200).send();
  } catch (error) {
    response.status(500).send(error);
  }
});

export {};
