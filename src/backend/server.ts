import cors from 'cors';
import express from 'express';
import connectDB from './config/db';

connectDB();

const router = require('./routes/')
const PORT = process.env.PORT || 5000;
const server = express();

server.use(cors());
server.listen(PORT); 
server.use('/', router)

server.get("/hello", (req, res) => {
    res.send("hello!!")
})

export default server;  
