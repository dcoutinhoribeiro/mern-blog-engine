import { createPost, getPosts, updatePost, deletePost } from '../../controllers/';
import express from 'express';
import bodyParser from 'body-parser';

const router = express.Router(); 
const jsonParser = bodyParser.json();
 
router.route('/post').get(getPosts);
router.post('/post', jsonParser, createPost);
router.patch("/post/:id", jsonParser, updatePost);
router.delete("/post/:id", deletePost);

module.exports = router;