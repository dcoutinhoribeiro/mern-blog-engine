import express, { Router } from 'express';
import fs from 'fs';
import path from 'path';

const router = express.Router();

const subRouters: Router[] = [];

const getAllSubroutes = (dir: string) => {
    fs.readdirSync(dir).forEach((file) => {
      const fullPath = path.join(dir, file);
      if (fs.lstatSync(fullPath).isDirectory()) {
        getAllSubroutes(fullPath);
      } else {
        if (fullPath !== __filename) {
          subRouters.push(require(fullPath));
        }
      }
      return subRouters;
    });
  };

getAllSubroutes(__dirname)
router.use('/', subRouters);

module.exports = router;